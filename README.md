# TEAM EVENTS


## Application Overall Architecture

[Here](img/app_arch.svg) is the overall architecture of the application.


You can find the controllers in `src/main/java/datahike/javademo/controller`,
the views under `src/main/resources/templates` and the main entry point in `src/main/java/datahike/javademo/JavaDemoApplication.java`.

You can find a blog post about Datahike and this application [here](https://www.lambdaforge.io/2020/05/25/java-api.html).

## How to run

``` sh
mvn clean install
```

to create the project jar. (You need maven.)

Then run the jar with:

``` sh
java -jar <path_to_created_jar>/java-demo-0.0.1-SNAPSHOT.jar  <your_datahike_db_url>
```


And visit your browser at [http://localhost:8080/teams](http://localhost:8080/teams) 
