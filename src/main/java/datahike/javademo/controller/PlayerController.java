package datahike.javademo.controller;

import clojure.lang.PersistentVector;
import datahike.java.Datahike;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

import static datahike.java.Datahike.dConn;
import static datahike.java.Util.*;
import static datahike.javademo.JavaDemoApplication.conn;

@Controller
public class PlayerController {
    @RequestMapping(path = "/players", method = RequestMethod.GET)
    public String index(Model model){
        String query = "[:find ?e ?n :in $ :where [?e :player/name ?n]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn));
        model.addAttribute("players", res);
        return "players/index";
    }

    @RequestMapping(path = "/players/new_player")
    public String new_player(Model model){
        return "players/new";
    }

    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public String create(Model model, @RequestParam("name") String name) {
        Datahike.transact(conn, vec(map(k(":player/name"), name)));
        return "redirect:/players";
    }

    @RequestMapping(path = "/events/{eventId}/players", method = RequestMethod.POST)
    public String create(Model model, @PathVariable("eventId") int eventId,
                         @RequestParam("playerId") int playerId,
                         @RequestParam("teamName") String teamName) {
        String query = "[" +
                ":find ?ti ?teamName " +
                ":in $ ?teamName " +
                ":where [?ti :team/name ?teamName]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn), teamName);
        int teamId = (int) res.iterator().next().get(0);

        Datahike.transact(conn,
                vec(vec(k(":db/add"), playerId, k(":player/event"), eventId),
                        vec(k(":db/add"), playerId, k(":player/team"), teamId)));
        return "redirect:/teams/" + teamName;
    }

    @RequestMapping(value = "/players/delete/{id}", method = RequestMethod.GET)
    public String delete(Model model, @PathVariable("id") int id) {
        Datahike.transact(conn, vec(vec(k(":db.fn/retractEntity"), id)));
        return "redirect:/players";
    }

    @RequestMapping(value = "/events/{eventId}/teams/{teamId}/players/delete/{playerId}", method = RequestMethod.GET)
    public String deleteFromEvent(Model model, @PathVariable("teamId") int teamId,
                                  @PathVariable("eventId") int eventId,
                                  @PathVariable("playerId") int playerId) {
        // NOTE: When we added a player we added to an event AND a team, but here when removing we remove only
        // from the event and not from the team. Removing from the team would break the other links the player might have with
        // other events.
        Datahike.transact(conn, vec(vec(k(":db/retract"), playerId, k(":player/event"), eventId)));

        String query = "[:find ?teamName :in $ ?ti :where [?ti :team/name ?teamName]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn), teamId);
        String teamName = (String) res.iterator().next().get(0);
        return "redirect:/teams/" + teamName;
    }

}
