package datahike.javademo.controller;

import clojure.lang.PersistentVector;
import datahike.java.Datahike;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

import static datahike.java.Datahike.*;
import static datahike.java.Util.*;
import static datahike.javademo.JavaDemoApplication.conn;

@Controller
public class EventController {
    @RequestMapping(path = "/events", method = RequestMethod.GET)
    public String index(Model model){
        String query = "[:find ?e ?n :in $ :where [?e :event/name ?n]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn));
        model.addAttribute("events", res);
        return "events/index";
    }

    @RequestMapping(path = "/events/new_event")
    public String new_event(Model model){
        return "events/new";
    }

    @RequestMapping(path = "/events", method = RequestMethod.POST)
    public String create(Model model, @RequestParam("name") String name) {
        Datahike.transact(conn, vec(map(k(":event/name"), name)));
        return "redirect:/events";
    }

    @RequestMapping(path = "/events/{eventId}", method = RequestMethod.POST)
    public String update(Model model, @PathVariable("eventId") String eventId,
                         @RequestParam("eventName") String name) {
        Datahike.transact(conn, vec(vec(k(":db/add"), Integer.parseInt(eventId), k(":event/name"), name)));
        return "redirect:/events";
    }

    @RequestMapping(path = "/events/{eventId}/edit", method = RequestMethod.GET)
    public String edit(Model model, @PathVariable("eventId") String eventId) {
        String query = "[:find ?n :in $ ?eventId :where [?eventId :event/name ?n]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn), Integer.parseInt(eventId));
        model.addAttribute("eventName", res.iterator().next().get(0));
        model.addAttribute("eventId", eventId);
        return "events/edit";
    }

    @RequestMapping(path = "/teams/{teamId}/events", method = RequestMethod.POST)
    public String add_event(Model model, @PathVariable("teamId") int teamId,
                            @RequestParam("teamName") String teamName,
                            @RequestParam("eventId") int eventId) {
        Datahike.transact(conn, vec(vec(k(":db/add"), teamId, k(":team/event"), eventId)));
        return "redirect:/teams/" + teamName;
    }

    @RequestMapping(value = "/events/delete/{id}", method = RequestMethod.GET)
    public String delete(Model model, @PathVariable("id") int id) {
        Datahike.transact(conn, vec(vec(k(":db/retractEntity"), id)));
        return "redirect:/events";
    }
    
    @RequestMapping(value = "/teams/{teamId}/events/delete/{eventId}", method = RequestMethod.GET)
    public String deleteFromTeam(Model model, @PathVariable("teamId") int teamId
            , @PathVariable("eventId") int eventId) {
        Datahike.transact(conn, vec(vec(k(":db/retract"), teamId, k(":team/event"), eventId)));

        String query = "[:find ?teamName :in $ ?ti :where [?ti :team/name ?teamName]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn), teamId);
        String teamName = (String) res.iterator().next().get(0);
        return "redirect:/teams/" + teamName;
    }
}
