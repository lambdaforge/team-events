package datahike.javademo.controller;

import clojure.lang.PersistentVector;
import datahike.java.Datahike;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static datahike.java.Datahike.dConn;
import static datahike.java.Util.*;
import static datahike.javademo.JavaDemoApplication.conn;

@Controller
public class TeamController {
    @RequestMapping(path = "/teams", method = RequestMethod.GET)
    public String index(Model model){
        String query = "[:find ?e ?n :in $ :where [?e :team/name ?n]]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn));
        model.addAttribute("teams", res);
        return "teams/index";
    }

    @RequestMapping(path = "/teams/{teamName}", method = RequestMethod.GET)
    public String show(Model model, @PathVariable("teamName") String teamName){
        // Event - Player
        String query = "[:find ?ei ?en ?pi ?pn :in $ ?team-name :where " +
                "[?ti :team/name ?team-name]" +
                "[?ti :team/event ?ei]" +
                "[?ei :event/name ?en]" +
                "[?pi :player/event ?ei]" +
                "[?pi :player/name ?pn]" +
                "[?pi :player/team ?ti]" +
                "]";
        Set<PersistentVector> res = Datahike.q(query, dConn(conn), teamName);

        // Group players by event
        Map eventsToPlayers = res.stream().collect(Collectors.groupingBy((PersistentVector vec) -> vec.get(1)));
        model.addAttribute("eventsToPlayers", eventsToPlayers);

        // Player names
        query = "[:find ?e ?n :in $ :where [?e :player/name ?n]]";
        Set<PersistentVector> players = Datahike.q(query, dConn(conn));
        model.addAttribute("players", players);

        // team
        query = "[:find ?ti :in $ ?teamName :where [?ti :team/name ?teamName]]";
        Set<PersistentVector> team = Datahike.q(query, dConn(conn), teamName);
        int teamId = (int) team.iterator().next().get(0);
        model.addAttribute("teamId", teamId);
        model.addAttribute("teamName", teamName);

        // Events for this team
        query = "[:find ?e ?n :in $ ?teamId :where " +
                "[?e :event/name ?n]" +
                "[?teamId :team/event ?e]" +
                "]";
        Set<PersistentVector> teamEvents = Datahike.q(query, dConn(conn), teamId);
        model.addAttribute("teamEvents", teamEvents);

        // All Events
        query = "[:find ?e ?n :in $ :where " +
                "[?e :event/name ?n]" +
                "]";
        Set<PersistentVector> allEvents = Datahike.q(query, dConn(conn));
        model.addAttribute("allEvents", allEvents);

        return "teams/show";
    }

    @RequestMapping(path = "/teams/new_team")
    public String new_team(Model model){
        return "teams/new";
    }

    @RequestMapping(path = "/teams", method = RequestMethod.POST)
    public String create(Model model, @RequestParam("name") String name) {
        Datahike.transact(conn, vec(map(k(":team/name"), name)));
        return "redirect:/teams";
    }

    @RequestMapping(value = "/teams/delete/{id}", method = RequestMethod.GET)
    public String delete(Model model, @PathVariable("id") int id) {
        Datahike.transact(conn, vec(vec(k(":db/retractEntity"), id)));
        return "redirect:/teams";
    }
}
