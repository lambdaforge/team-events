package datahike.javademo;

import clojure.java.api.Clojure;
import datahike.java.Datahike;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static datahike.java.Util.*;

@SpringBootApplication
public class JavaDemoApplication {
	public static String uri;
	public static Object conn;


	private static void initDB() {
		Object schema = Clojure.read(" [{:db/ident :team/name\n" +
				"                 :db/valueType :db.type/string\n" +
				"                 :db/unique :db.unique/identity\n" +
				"                 :db/index true\n" +
				"                 :db/cardinality :db.cardinality/one}\n" +

				"                {:db/ident :team/event\n" +
				"                 :db/valueType :db.type/ref\n" +
				"                 :db/cardinality :db.cardinality/many}" +

				"                {:db/ident :event/name\n" +
				"                 :db/valueType :db.type/string\n" +
				"                 :db/unique :db.unique/identity\n" +
				"                 :db/cardinality :db.cardinality/one}" +

				"                {:db/ident :player/name\n" +
				"                 :db/valueType :db.type/string\n" +
				"                 :db/unique :db.unique/identity\n" +
				"                 :db/cardinality :db.cardinality/one}" +

				"                {:db/ident :player/event\n" +
				"                 :db/valueType :db.type/ref\n" +
				"                 :db/cardinality :db.cardinality/many}" +

				"                {:db/ident :player/team\n" +
				"                 :db/valueType :db.type/ref\n" +
				"                 :db/cardinality :db.cardinality/many}" +

				"]");

		Datahike.deleteDatabase(uri);
		Datahike.createDatabase(uri, k(":initial-tx"), schema);

		conn = Datahike.connect(uri);

		// Add some initial example data
		//
		String geneva_event = "Geneva";
		String cortina_event = "Cortina";
		Datahike.transact(conn, vec(
				map(k(":event/name"), geneva_event),
				map(k(":event/name"), cortina_event)));

		String jane = "Jane";
		String paul = "Paul";
		Datahike.transact(conn, vec(
				map(k(":player/name"), jane),
				map(k(":player/name"), paul)));

		String zurich_team = "Zurich Peelers";
		String geneva_team = "Geneva Lully";
		Datahike.transact(conn, vec(
				map(k(":team/name"), zurich_team),
				map(k(":db/id"), vec(k(":team/name"), zurich_team), k(":team/event"), vec(k(":event/name"), geneva_event)),
				map(k(":db/id"), vec(k(":team/name"), zurich_team), k(":team/event"), vec(k(":event/name"), cortina_event)),
				map(k(":db/id"), vec(k(":player/name"), paul), k(":player/event"), vec(k(":event/name"), cortina_event)),
				map(k(":db/id"), vec(k(":player/name"), paul), k(":player/team"), vec(k(":team/name"), zurich_team)),
				map(k(":db/id"), vec(k(":player/name"), jane), k(":player/event"), vec(k(":event/name"), cortina_event)),
				map(k(":db/id"), vec(k(":player/name"), jane), k(":player/team"), vec(k(":team/name"), zurich_team)),
				map(k(":db/id"), vec(k(":player/name"), paul), k(":player/event"), vec(k(":event/name"), geneva_event)),
				map(k(":db/id"), vec(k(":player/name"), paul), k(":player/event"), vec(k(":team/name"), zurich_team)),

				map(k(":team/name"), geneva_team),
				map(k(":db/id"), vec(k(":team/name"), geneva_team), k(":team/event"), vec(k(":event/name"), geneva_event)),
				map(k(":db/id"), vec(k(":player/name"), paul), k(":player/event"), vec(k(":event/name"), geneva_event)),
				map(k(":db/id"), vec(k(":player/name"), paul), k(":player/team"), vec(k(":team/name"), geneva_team))

				));
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Expecting db uri as argument");
			return;
		}

		// Expects something like for args[0]: datahike:file:///Users/paul/temp/events-db-2
		uri = args[0];

		if (!Datahike.databaseExists(uri)) {
			System.out.println("Creating new db: " + uri);
			initDB();
		}

		System.out.println("Starting with db: " + uri);

		conn = Datahike.connect(uri);
		SpringApplication.run(JavaDemoApplication.class, args);
	}

}
