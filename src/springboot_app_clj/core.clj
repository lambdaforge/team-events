(ns springboot-app-clj.core)


(require '[datahike.api :as d])

(def uri "datahike:file:///Users/paul/temp/events-db")


(def conn (d/connect uri))


(d/q '[:find ?ei ?en :in $
       :where [?ei :event/name ?en]]
     @conn)


(d/q '[:find ?pi ?ei :in $ :where
       [?pi :player/name "magnus 2"]
       [?pi :player/event ?ei]
       ;;[?pi :player/team ?ti]
       ]
     @conn)


(d/q '[:find ?n :in $ ?eventId :where [?eventId :event/name ?n]]
     @conn, 31)
